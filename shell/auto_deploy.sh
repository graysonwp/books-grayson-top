#!bin/bash

# 常量定义
# 项目根目录
BASE_DIR=/Users/weipeng/Personal/Projects/books.grayson.top

echo "0. 进入项目根目录"
cd ${BASE_DIR}

echo "1. 生成静态文件"
cd ${BASE_DIR}/exampleSite
hugo --themesDir ../.. -b https://books.grayson.top
cd ${BASE_DIR}
git add -A ./
git commit -a -m 自动提交
git pull
git push origin master

echo "2. 上传到服务器"
cd ${BASE_DIR}/exampleSite
scp -r public/* root@cloudserver://www/wwwroot/books.grayson.top/
cd ${BASE_DIR}
